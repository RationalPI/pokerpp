#pragma once

#include "I_tool.h"
#include "projectcontext.h"

class OpenCV_Panel;
struct OpenCV:public I_Tool{
	void previewProcess();
	void applyProcess();
	void previewRestor();
private:
	std::shared_ptr<OpenCV_Panel> dockForm;
	PCIT_I *pc=nullptr;
	void process(std::shared_ptr<PainterProxy> painter);

	void enable(bool en, PCIT_I *pc)override;
	QIcon icone()override{
		return QIcon(":/icone/ocv.png");
	}
	QString stringID()override{return "OpenCV";}
};

