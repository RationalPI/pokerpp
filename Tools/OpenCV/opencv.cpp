#include "opencv.h"
#include "qtopencv.h"
#include <QLabel>
#include <QMainWindow>
#include "opencv_toolform.h"

struct OpenCV_Panel:public QGraphicsSimpleTextItem,public QObject{
	OpenCV_ToolForm* form=nullptr;
	OpenCV_Panel(PCIT_I *pc,OpenCV *ownerTool){
		dockWidget = std::make_shared<QDockWidget>(tr("OpenCV functions"));{
			form=new OpenCV_ToolForm(ownerTool);
			connect(form,&QObject::destroyed,[this](){
				form=nullptr;
			});
			dockWidget->setWidget(form);
			dockWidget->setAllowedAreas(Qt::RightDockWidgetArea);
			auto w=dynamic_cast<QMainWindow*>(pc->window());assert(w);
			w->addDockWidget(Qt::RightDockWidgetArea, dockWidget.get());
		}
	}
private:
	std::shared_ptr<QDockWidget> dockWidget;
};

void OpenCV::enable(bool en, PCIT_I *pc){
	if(en){
		dockForm=std::make_shared<OpenCV_Panel>(pc,this);
		this->pc=pc;
	}
	else{
		pc->sel().flushOverlay();
		dockForm.reset();
		this->pc=nullptr;
	}
}

void OpenCV::previewProcess(){
	pc->sel().flushOverlay();
	pc->sel().setOverlayOpacity(1);
	process(pc->sel().painterOverlay());
}
void OpenCV::applyProcess(){
	pc->sel().flushOverlay();
	process(pc->currentLayerPainterSelFiltered());
}
void OpenCV::previewRestor(){
	pc->sel().flushOverlay();
	pc->scene()->update();
}

void OpenCV::process(std::shared_ptr<PainterProxy> painter){
	auto& params=dockForm->form->params();
	switch (params.tab) {
	case Parameters::Tab::Canny:{
		auto in=pc->currentLayer().convertToFormat(QImage::Format_Grayscale8);
		auto out=in.copy();
		cv::Canny(
					QtOpenCV::m_ref(in,CV_8U),
					QtOpenCV::m_ref(out,CV_8U),
					params.canny.t1,
					params.canny.t2
					);
		painter->drawImage(QPoint(),out);

	}break;
	case Parameters::Tab::MedianBlur:{
		auto out=pc->currentLayer().copy();
		cv::medianBlur(
					QtOpenCV::m_ref(pc->currentLayer(),CV_8UC4),
					QtOpenCV::m_ref(out,CV_8UC4),
					params.medianBlur.ksize
					);
		painter->drawImage(QPoint(),out);
	}break;
	case Parameters::Tab::GaussianBlur:{
		auto out=pc->currentLayer().copy();
		cv::GaussianBlur(
					QtOpenCV::m_ref(pc->currentLayer(),CV_8UC4),
					QtOpenCV::m_ref(out,CV_8UC4),
					cv::Size(params.gaussianBlur.ksizeX,params.gaussianBlur.ksizeY),
					params.gaussianBlur.sigmaX,
					params.gaussianBlur.sigmaX
					);
		painter->drawImage(QPoint(),out);
	}break;
	}
	pc->scene()->update();
}
