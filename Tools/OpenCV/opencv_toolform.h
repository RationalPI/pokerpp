#pragma once

#include <QWidget>
#include "opencv.h"

namespace Ui {
class OpenCV_ToolForm;
}

struct Parameters{
	enum class Tab{
		Canny,
		GaussianBlur,
		MedianBlur,
	}tab=Tab::Canny;
	struct Canny{
		double t1=20;
		double t2=50;
	}canny;
	struct GaussianBlur{
		int ksizeX=11,ksizeY=11;
		double sigmaX,sigmaY;
		int borderType=4/*BORDER_REFLECT_101 */;
	}gaussianBlur;
	struct MedianBlur{
		int ksize=11;
	}medianBlur;
};

struct Parameters;
class OpenCV_ToolForm : public QWidget{
	Q_OBJECT
	Ui::OpenCV_ToolForm *ui;
	OpenCV* ownerTool;
	Parameters parameters;
public:
	explicit OpenCV_ToolForm(OpenCV *ownerTool, QWidget *parent = nullptr);
	~OpenCV_ToolForm();
	const Parameters& params(){return parameters;}
	void restorFromParams();
	bool eventFilter(QObject *o, QEvent *e);
};
