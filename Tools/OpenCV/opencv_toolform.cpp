#include "opencv_toolform.h"
#include "ui_opencv_toolform.h"

OpenCV_ToolForm::OpenCV_ToolForm(OpenCV* ownerTool,QWidget *parent) :
	QWidget(parent),
	ui(new Ui::OpenCV_ToolForm),
	ownerTool(ownerTool)
{
	ui->setupUi(this);

	restorFromParams();

	connect(ui->prev,&QPushButton::pressed,[&](){
		this->ownerTool->previewProcess();
	});
	connect(ui->undoPrev,&QPushButton::pressed,[&](){
		this->ownerTool->previewRestor();
	});
	connect(ui->apply,&QPushButton::pressed,[&](){
		this->ownerTool->applyProcess();
	});
	//###########################################################################
	//###########################################################################
	//###########################################################################
	connect(ui->selector,QOverload<int>::of(&QComboBox::activated),[&](int activatedIndex){
		parameters.tab=(Parameters::Tab)activatedIndex;
		ui->stackedWidget->setCurrentIndex(activatedIndex);
		this->ownerTool->previewProcess();
	});
	//###########################################################################
	//###########################################################################
	//###########################################################################
	connect(ui->canny_t1,QOverload<double>::of(&QDoubleSpinBox::valueChanged),[&](double v){
		parameters.canny.t1=v;
		this->ownerTool->previewProcess();
	});
	connect(ui->canny_t2,QOverload<double>::of(&QDoubleSpinBox::valueChanged),[&](double v){
		parameters.canny.t2=v;
		this->ownerTool->previewProcess();
	});
	//###########################################################################
	//###########################################################################
	//###########################################################################
	connect(ui->GaussianBlur_ksizeX,QOverload<int>::of(&QSpinBox::valueChanged),[&](double v){
		parameters.gaussianBlur.ksizeX=v;
		this->ownerTool->previewProcess();
	});
	connect(ui->GaussianBlur_ksizeY,QOverload<int>::of(&QSpinBox::valueChanged),[&](double v){
		parameters.gaussianBlur.ksizeY=v;
		this->ownerTool->previewProcess();
	});
	connect(ui->GaussianBlur_sigmaX,QOverload<double>::of(&QDoubleSpinBox::valueChanged),[&](double v){
		parameters.gaussianBlur.sigmaX=v;
		this->ownerTool->previewProcess();
	});
	connect(ui->GaussianBlur_sigmaY,QOverload<double>::of(&QDoubleSpinBox::valueChanged),[&](double v){
		parameters.gaussianBlur.sigmaY=v;
		this->ownerTool->previewProcess();
	});
	connect(ui->GaussianBlur_borderType,QOverload<int>::of(&QComboBox::activated),[&](int index){
		parameters.gaussianBlur.borderType=index;
		this->ownerTool->previewProcess();
	});
	//###########################################################################
	//###########################################################################
	//###########################################################################
	connect(ui->MedianBlur_ksize,QOverload<int>::of(&QSpinBox::valueChanged),[&](double v){
		parameters.medianBlur.ksize=v;
		this->ownerTool->previewProcess();
	});



	/*we don't whant the kernel size to be set manualy to disable odd values*/{
		ui->GaussianBlur_ksizeX->installEventFilter(this);
		ui->GaussianBlur_ksizeY->installEventFilter(this);
		ui->MedianBlur_ksize->installEventFilter(this);
	}


}

bool OpenCV_ToolForm::eventFilter(QObject *o, QEvent *e){
	if (dynamic_cast<QSpinBox*>(o)&& e->type() == QEvent::KeyPress) {
		/*we don't whant the kernels sizes to be set manualy to disable odd values*/
		e->ignore();
		return true;
	}
	return QWidget::eventFilter(o, e);
}

void OpenCV_ToolForm::restorFromParams(){
	ui->canny_t1->setValue(parameters.canny.t1);
	ui->canny_t2->setValue(parameters.canny.t2);

	ui->GaussianBlur_ksizeX->setValue(parameters.gaussianBlur.ksizeX);
	ui->GaussianBlur_ksizeY->setValue(parameters.gaussianBlur.ksizeY);
	ui->GaussianBlur_sigmaX->setValue(parameters.gaussianBlur.sigmaX);
	ui->GaussianBlur_sigmaY->setValue(parameters.gaussianBlur.sigmaY);
	ui->GaussianBlur_borderType->setCurrentIndex(parameters.gaussianBlur.borderType);

	ui->MedianBlur_ksize->setValue(parameters.medianBlur.ksize);
}

OpenCV_ToolForm::~OpenCV_ToolForm(){
	delete ui;
}


