#pragma once

#include "cards.h"

#include <QFuture>

struct CardImageAnalyzer{
    CardImageAnalyzer();
    std::map<Cards::Color::value_type,cv::Mat> colorImagesTable;//! the images that can be compared against for colors on table
    std::map<Cards::Color::value_type,std::vector<cv::Mat>> colorImagesHand;//! the images that can be compared against for colors in hand
    std::map<Cards::Value,cv::Mat> valueImagesTable;//! the images that can be compared against for values on table
    std::map<Cards::Value,std::vector<cv::Mat>> valueImagesHand;//! the images that can be compared against for values in hand
    [[nodiscard]] QFuture<std::set<Cards::Card>> table(const cv::Mat& capturedGray,cv::Mat& infoImage)const;
    [[nodiscard]] QFuture<std::set<Cards::Card>> hand(const cv::Mat& capturedGray,cv::Mat& infoImage)const;
};
