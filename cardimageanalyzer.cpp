#include "cardimageanalyzer.h"

#include <QtConcurrentMap>
#include <opencv2/opencv.hpp>
#include <QPainter>
#include <optional>

void generateCardImagesFromTemplate(){
    std::string path("C:\\Users\\1salm\\Documents\\PockerPP\\cards\\");

    for (int i = 2; i < 15; ++i) {
        auto val=cv::imread(path+(QString::number(i)+".png").toStdString(),cv::IMREAD_UNCHANGED);

        /*add aplha to val*/{
            std::vector<cv::Mat> chanles;
            cv::split(val,chanles);
            chanles.push_back(chanles.front().clone());
            chanles.back().setTo(255);
            cv::merge(chanles,val);
        }

        for (auto color : Cards::Color::values) {
            auto col=cv::imread(path+color+".png",cv::IMREAD_UNCHANGED);
            auto res=QtOpenCV::q_cpy(val,QImage::Format::Format_ARGB32);
            auto p=std::make_shared<QPainter>(&res);
            p->drawImage(0,0,QtOpenCV::q_ref(col,QImage::Format::Format_ARGB32));

            cv::imwrite(path+"\\a\\"+(QString::number(i)+"_"+color+".png").toStdString(),QtOpenCV::m_ref(res,QtOpenCV::FORMAT_BGRA));
        }
    }
}



CardImageAnalyzer::CardImageAnalyzer(){
    assert(QtOpenCV::platformFormat()==QtOpenCV::QImage32BitsFromat::BGRA
           &&"unsuported platform for this tool");

    for(auto& card:Cards::Color::values){
        colorImagesTable[card]=Cards::colorImage(card);
    }

    for(auto& val:Cards::values){
        valueImagesTable[val]=Cards::valueImage(val);
    }

    //rotation de 8 degres && 15% scale
    static const auto scaleFactor=1/1.14;
    static const auto angle=6;
    static const auto bkgColor=cv::Scalar(255,255,255,255);
    for(auto& card:Cards::Color::values){
        cv::Mat scaled,scaledRotated,scaledRotatedFliped;

        cv::resize(Cards::colorImage(card),scaled,{0,0},scaleFactor,scaleFactor);

        auto rotMatrix=cv::getRotationMatrix2D(cv::Point2f(scaled.cols,scaled.rows)/2,angle,1);
        cv::warpAffine(scaled,scaledRotated,rotMatrix,{scaled.cols,scaled.rows},cv::INTER_LINEAR,cv::BORDER_CONSTANT,bkgColor);

        cv::flip(scaledRotated,scaledRotatedFliped,1);

        colorImagesHand[card].push_back(scaledRotated);
        colorImagesHand[card].push_back(scaledRotatedFliped);
    }
    for(auto& val:Cards::values){
        cv::Mat scaled,scaledRotated,scaledRotatedOther;

        cv::resize(Cards::valueImage(val),scaled,{0,0},scaleFactor,scaleFactor);

        auto rotMatrix=cv::getRotationMatrix2D(cv::Point2f(scaled.cols,scaled.rows)/2,angle,1);
        cv::warpAffine(scaled,scaledRotated,rotMatrix,{scaled.cols,scaled.rows},cv::INTER_LINEAR,cv::BORDER_CONSTANT,bkgColor);

        rotMatrix=cv::getRotationMatrix2D(cv::Point2f(scaled.cols,scaled.rows)/2,-angle,1);
        cv::warpAffine(scaled,scaledRotatedOther,rotMatrix,{scaled.cols,scaled.rows},cv::INTER_LINEAR,cv::BORDER_CONSTANT,bkgColor);

        valueImagesHand[val].push_back(scaledRotated);
        valueImagesHand[val].push_back(scaledRotatedOther);
    }
}

constexpr auto xOff=-45;
constexpr auto yOff=-85;
constexpr auto w=105;
constexpr auto h=100;


const auto font = cv::FONT_HERSHEY_SIMPLEX;
const auto font_size = 0.7;
const auto font_color = cv::Scalar(0,0,0,255);
const auto font_thickness = 1;

inline double percent(float v){
    return 1-(v/100);
}


std::function<void(std::set<Cards::Card>&,const std::set<Cards::Card>&)> cardReducer=
        [](std::set<Cards::Card>& a,const std::set<Cards::Card>& b){
    std::copy(b.begin(),b.end(),std::inserter(a,a.end()));
};

QFuture<std::set<Cards::Card>> CardImageAnalyzer::table(const cv::Mat &capturedGray, cv::Mat &infoImage) const{
    static const cv::Rect tableRoic(1000,585,600,90); cv::rectangle(infoImage, tableRoic, cv::Scalar(0,0,0,255), 2, 8, 0 );
    std::function<std::set<Cards::Card>(Cards::Color::value_type&)> table=[&](const Cards::Color::value_type& color){
        std::set<Cards::Card> ret;
        cv::Mat matchResult;
        cv::matchTemplate(capturedGray(tableRoic),colorImagesTable.at(color),matchResult,cv::TM_SQDIFF_NORMED);

        for(int k=1;k<=6;k++){
            double minVal;
            cv::Point minLoc,maxLoc;
            cv::minMaxLoc( matchResult, &minVal, 0, &minLoc, &maxLoc, cv::Mat() );
            cv::circle(matchResult,minLoc,colorImagesTable.at(Cards::Color::Carre).cols/4, 0.5,cv::FILLED);// ban found zone from being found again
            cv::circle(matchResult,maxLoc,colorImagesTable.at(Cards::Color::Carre).cols/4, 0.5,cv::FILLED);// ban found zone from being found again

            minLoc.x=minLoc.x+tableRoic.x;
            minLoc.y=minLoc.y+tableRoic.y;

            if(minVal < percent(98)) /*1 - 0.065 = 0.935 : 93.5% */{
                /*decorating*/{
                    maxLoc.x = minLoc.x + colorImagesTable.at(color).cols;
                    maxLoc.y = minLoc.y + colorImagesTable.at(color).rows;
                    cv::rectangle(
                                infoImage,
                                minLoc,
                                maxLoc,
                                cv::Scalar(0,255,0,255),
                                2,
                                8,
                                0
                                );

                    auto txt=QString(color)+"|"+QString::number(100.0*(1-minVal),'f',1)+"%";
                    cv::putText(infoImage,txt.toStdString(),maxLoc,font, font_size, font_color, font_thickness, cv::LINE_AA);
                }

                /*finding value*/{
                    cv::Rect thatCardRoi(minLoc.x+xOff,minLoc.y+yOff,w,h); cv::rectangle(infoImage, thatCardRoi, cv::Scalar(0,0,0,255), 2, 8, 0 );
                    cv::Mat matchResult;
                    for (auto& value : Cards::values) {
                        cv::matchTemplate(capturedGray(thatCardRoi),valueImagesTable.at(value),matchResult,cv::TM_SQDIFF_NORMED);
                        double minVal;
                        cv::Point minLoc,maxLoc;
                        cv::minMaxLoc( matchResult, &minVal, 0, &minLoc, &maxLoc, cv::Mat() );

                        minLoc.x=minLoc.x+thatCardRoi.x;
                        minLoc.y=minLoc.y+thatCardRoi.y;

                        if(minVal < percent(97)) /*1 - 0.065 = 0.935 : 93.5% */{
                            /*decorating*/{
                                maxLoc.x = minLoc.x + valueImagesTable.at(value).cols;
                                maxLoc.y = minLoc.y + valueImagesTable.at(value).rows;
                                cv::rectangle(
                                            infoImage,
                                            minLoc,
                                            maxLoc,
                                            cv::Scalar(0,255,0,255),
                                            2,
                                            8,
                                            0
                                            );

                                auto txt=QString::number(value)+"|"+QString::number(100.0*(1-minVal),'f',1)+"%";
                                cv::putText(infoImage,txt.toStdString(),maxLoc,font, font_size, font_color, font_thickness, cv::LINE_AA);
                            }
                            ret.insert({value,color});
                            break;
                        }
                    }
                }
            }
        }

        return ret;
    };
    return QtConcurrent::mappedReduced<std::set<Cards::Card>>(Cards::Color::values,table,cardReducer);
}

QFuture<std::set<Cards::Card>> CardImageAnalyzer::hand(const cv::Mat &capturedGray, cv::Mat &infoImage) const{
    static const cv::Rect handRoic(1210,885,400,90); cv::rectangle( infoImage, handRoic, cv::Scalar(0,0,0,255), 2, 8, 0 );
    std::function<std::set<Cards::Card>(Cards::Color::value_type&)> hand=[&](Cards::Color::value_type& color){
        std::set<Cards::Card> ret;

        auto a=0;
        while (true) {
            a++;
        }
        for(auto& templateMat:colorImagesHand.at(color)){
            cv::Mat matchResult;
            cv::matchTemplate(capturedGray(handRoic),templateMat,matchResult,cv::TM_SQDIFF_NORMED);

            double minVal;
            cv::Point minLoc,maxLoc;
            // no need to find multiple instances of one color as there is a 16° angles btw the two cards and we have one sprit for eatch side
            cv::minMaxLoc( matchResult, &minVal, 0, &minLoc, &maxLoc, cv::Mat() );

            minLoc.x=minLoc.x+handRoic.x;
            minLoc.y=minLoc.y+handRoic.y;

            if(minVal < percent(97)) /*1 - 0.065 = 0.935 : 93.5% */{
                /*decorating*/{
                    maxLoc.x = minLoc.x + templateMat.cols;
                    maxLoc.y = minLoc.y + templateMat.rows;
                    cv::rectangle(
                                infoImage,
                                minLoc,
                                maxLoc,
                                cv::Scalar(0,255,0,255),
                                2,
                                8,
                                0
                                );

                    auto txt=QString(color)+"|"+QString::number(100.0*(1-minVal),'f',1)+"%";
                    cv::putText(infoImage,txt.toStdString(),maxLoc,font, font_size, font_color, font_thickness, cv::LINE_AA);
                }

                /*finding value*/{
                    cv::Rect thatCardRoi(minLoc.x+xOff,minLoc.y+yOff,w,h); cv::rectangle(infoImage, thatCardRoi, cv::Scalar(0,0,0,255), 2, 8, 0 );
                    cv::Mat matchResult;
                    std::optional<Cards::Value> foundValue;
                    for (auto& value : Cards::values) {if(foundValue)break;
                        for (auto& valmat : valueImagesHand.at(value)) {if(foundValue)break;
                            cv::matchTemplate(capturedGray(thatCardRoi),valmat,matchResult,cv::TM_SQDIFF_NORMED);
                            double minVal;
                            cv::Point minLoc,maxLoc;
                            cv::minMaxLoc( matchResult, &minVal, 0, &minLoc, &maxLoc, cv::Mat() );

                            minLoc.x=minLoc.x+thatCardRoi.x;
                            minLoc.y=minLoc.y+thatCardRoi.y;

                            if(minVal < percent(96)) /*1 - 0.065 = 0.935 : 93.5% */{
                                foundValue=value;
                                /*decorating*/{
                                    maxLoc.x = minLoc.x + valmat.cols;
                                    maxLoc.y = minLoc.y + valmat.rows;
                                    cv::rectangle(
                                                infoImage,
                                                minLoc,
                                                maxLoc,
                                                cv::Scalar(0,255,0,255),
                                                2,
                                                8,
                                                0
                                                );

                                    auto txt=QString::number(value)+"|"+QString::number(100.0*(1-minVal),'f',1)+"%";
                                    cv::putText(infoImage,txt.toStdString(),maxLoc,font, font_size, font_color, font_thickness, cv::LINE_AA);
                                }
                            }
                        }
                    }
                    if(foundValue)ret.insert({foundValue.value(),color});
                }
            }
        }
        return ret;
    };
    return QtConcurrent::mappedReduced<std::set<Cards::Card>>(Cards::Color::values,hand,cardReducer);
}
