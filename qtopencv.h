#pragma once

#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>
#include <QImage>


namespace QtOpenCV{
constexpr auto FORMAT_BGRA=CV_8UC4;
constexpr auto FORMAT_BGR=CV_8UC3;

inline cv::Mat m_ref(const QImage &img, int format){
	return cv::Mat(img.height(), img.width(),
						format, (char*)img.bits(), img.bytesPerLine());
}
inline cv::Mat m_cpy(QImage const &img, int format){
	return cv::Mat(img.height(), img.width(), format,
						const_cast<uchar*>(img.bits()),
						img.bytesPerLine()).clone();
}
inline QImage q_ref(cv::Mat &mat, QImage::Format format){
	return QImage(mat.data, mat.cols, mat.rows, mat.step, format);
}
inline QImage q_cpy(cv::Mat const &mat, QImage::Format format){
	return QImage(mat.data, mat.cols, mat.rows, mat.step, format).copy();
}

template <typename T>
cv::Mat bitwiseAnd(T& a,T& b) {
	cv::Mat out;
	cv::bitwise_and(a,b,out);
	return out;
}
template <typename T, typename... Rest>
cv::Mat bitwiseAnd(T& t, Rest... rest) {
	cv::Mat out;
	cv::bitwise_and(t,bitwiseAnd(rest...),out);
	return out;
}

enum class QImage32BitsFromat{
	ARGB,
	BGRA,
	UNKNOWN,
};
inline QImage32BitsFromat platformFormat(){
	/* QT documentation
			* "Warning: If you are accessing 32-bpp image data, cast the returned pointer
			* to QRgb* (QRgb has a 32-bit size) and use it to read/write the pixel value.
			* You cannot use the uchar* pointer directly, because the pixel format depends
			* on the byte order on the underlying platform. Use qRed(), qGreen(), qBlue(),
			* and qAlpha() to access the pixels."
*/

    QImage tmp(1,1,QImage::Format_ARGB32);
	tmp.setPixelColor(0,0,QColor(1,2,3,4));

	auto pixelData=(uint8_t*)tmp.bits();
	if(pixelData[0]==4&&pixelData[1]==1&&pixelData[2]==2&&pixelData[3]==3)
		return QImage32BitsFromat::ARGB;
	if(pixelData[0]==3&&pixelData[1]==2&&pixelData[2]==1&&pixelData[3]==4)
		return QImage32BitsFromat::BGRA;

	return QImage32BitsFromat::UNKNOWN;
}


inline cv::Point pt(const QPoint& pt){
	return cv::Point(pt.x(),pt.y());
}
inline cv::Point pt(const QPointF& pt){
	return cv::Point(pt.x(),pt.y());
}
inline cv::Scalar rgb(const QColor& col){
	int r,g,b;
	col.getRgb(&r,&g,&b);
	return cv::Scalar(r,g,b);
}
inline cv::Scalar bgr(const QColor& col){
	int r,g,b;
	col.getRgb(&r,&g,&b);
	return cv::Scalar(b,g,r);
}
inline cv::Scalar bgra(const QColor& col){
	int r,g,b,a;
	col.getRgb(&r,&g,&b,&a);
	return cv::Scalar(b,g,r,a);
}
};


