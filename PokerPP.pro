QT += core widgets concurrent
TARGET = PockerPP
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++17

!win32-msvc*: QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter

SOURCES += \
    cardimageanalyzer.cpp \
    cards.cpp \
    cv_screencapture.cpp \
    mainwin.cpp \
    qtopencv.cpp \


HEADERS += \
    cardimageanalyzer.h \
    cards.h \
    cv_screencapture.h \
    mainwin.h \
    qtopencv.h \

FORMS += \
    mainwin.ui \

INCLUDEPATH += 3rdParty/build/OpenCV/install/include/opencv4
INCLUDEPATH += 3rdParty/build/OpenCV/install/include
win32{
    msvc*: LIBS += -L$$PWD/3rdParty/build/OpenCV/install/x64/vc16/lib
    else:  LIBS += -L$$PWD/3rdParty/build/OpenCV/install/x64/mingw/lib
    LIBS += -lopencv_core420 -lopencv_imgproc420 -lopencv_highgui420 -lopencv_imgcodecs420 -lgdi32
}else{
    LIBS += -L$$PWD/3rdParty/build/OpenCV/install/lib -lopencv_core -lopencv_imgproc -lopencv_highgui
}

target.path = /bin
INSTALLS += target

RESOURCES += \
    cardsImages.qrc

include($$PWD/3rdParty/podds-master/podds.pri)
