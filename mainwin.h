#pragma once

#include <QMainWindow>
#include <memory>
#include <odds.h>

namespace Ui {
class MainWin;
}

class MainWin : public QMainWindow{
	Q_OBJECT
public:
	explicit MainWin(QWidget *parent = nullptr);
	~MainWin();

private:
	Ui::MainWin *ui;
    std::shared_ptr<std::vector<PooID>> cache;
};
