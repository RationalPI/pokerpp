#include "cards.h"

uint32_t Cards::cardToPoodID(Cards::Card c){
    // Create a Hand from a card. CardIdx is an integer between 0 and 51, so that CARD = 4 * RANK + SUIT, where
    // rank ranges from 0 (deuce) to 12 (ace) and suit is from 0 (spade) to 3 (diamond).
    uint32_t rank=c.first-Cards::Value::_2;
    uint32_t suit=0;
    if(c.second==Cards::Color::Carre){
        suit+=0;
    }else if(c.second==Cards::Color::Coeur){
        suit+=1;
    }else if(c.second==Cards::Color::Pick){
        suit+=2;
    }else if(c.second==Cards::Color::Trefle){
        suit+=3;
    }

    return suit*13 + rank;
}
