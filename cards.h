#pragma once

#include "qtopencv.h"
#include <set>

namespace Cards {
enum Value{
    _A=14,
    _K=13,
    _Q=12,
    _J=11,
    _10=10,
    _9=9,
    _8=8,
    _7=7,
    _6=6,
    _5=5,
    _4=4,
    _3=3,
    _2=2,
};
static const std::vector<Value> values={Value(2),Value(3),Value(4),Value(5),Value(6),Value(7),Value(8),Value(9),Value(10),Value(11),Value(12),Value(13),Value(14)};
namespace Color{
static constexpr char Carre='S';
static constexpr char Trefle='T';
static constexpr auto Coeur='C';
static constexpr char Pick='P';
static constexpr auto values={Carre,Trefle,Coeur,Pick};
using value_type=decltype (Carre);
};
using Card=std::pair<Cards::Value,Cards::Color::value_type>;
inline std::set<Card> allCards(){
    std::set<Card> ret;
    for (int val = 2; val < 15; ++val) {
        for (auto color : Cards::Color::values) {
            ret.insert({Cards::Value(val),color});
        }
    }
    return ret;
}
inline cv::Mat cardImage(const Card& card){
    auto resourcePath=QString(":/cards/cards/Images/")+QString::number(card.first)+"_"+card.second+".png";
    cv::Mat ret;
    cv::cvtColor(QtOpenCV::m_ref(QImage(resourcePath),QtOpenCV::FORMAT_BGRA),ret, cv::COLOR_BGRA2GRAY);
    return ret;
}
inline cv::Mat colorImage(const Cards::Color::value_type& card){
    auto resourcePath=QString(":/cards/cards/template/")+card+".png";
    cv::Mat ret;
    auto test=QImage(resourcePath);
    cv::cvtColor(QtOpenCV::m_ref(QImage(resourcePath),QtOpenCV::FORMAT_BGRA),ret, cv::COLOR_BGRA2GRAY);
    return ret;
}
inline cv::Mat valueImage(const Cards::Value& card){
    auto resourcePath=QString(":/cards/cards/template/")+QString::number(card)+".png";
    cv::Mat ret;
    cv::cvtColor(QtOpenCV::m_ref(QImage(resourcePath),QtOpenCV::FORMAT_BGRA),ret, cv::COLOR_BGRA2GRAY);
    return ret;
}
uint32_t cardToPoodID(Card c);
};
