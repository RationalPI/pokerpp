#pragma once

#include <opencv2\opencv.hpp>
#include <Windows.h>

class CV_ScreenCapture
{
public:
    CV_ScreenCapture(int displayIndex = -1);
    ~CV_ScreenCapture();

    void open(int displayIndex);
    void readTo(cv::Mat& destination);
    CV_ScreenCapture& operator>>(cv::Mat& destination);

private:
    cv::Rect2d captureArea;
    HWND targetWindow = NULL;


    void captureHwnd(HWND window, cv::Rect2d targetArea, cv::Mat& dest);
    static BOOL CALLBACK monitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData);
};
