#include <QApplication>


#include "mainwin.h"
#include "ui_mainwin.h"

#include "cv_screencapture.h"
#include "cardimageanalyzer.h"

#include <iostream>


#include <QScreen>
#include <QLabel>
#include <QTimer>

template<class T>
struct Combinations{
    static std::vector<std::vector<T>> values(const std::vector<T> &elems, unsigned long req_len){
        assert(req_len > 0 && req_len <= elems.size());
        std::vector<unsigned long> positions(req_len, 0);
        std::vector<std::vector<T>> ret;
        combinations_recursive(elems, req_len, positions, 0, 0,ret);
        return ret;
    }
private:
    static void combinations_recursive(const std::vector<T> &elems, unsigned long req_len,
                                       std::vector<unsigned long> &pos, unsigned long depth,
                                       unsigned long margin,std::vector<std::vector<T>>& ret){
        // Have we selected the number of required elements?
        if (depth >= req_len) {
            std::vector<T> suite;
            for (unsigned long ii = 0; ii < pos.size(); ++ii){
                suite.push_back(elems[pos[ii]]);
            }
            ret.push_back(suite);
            return;
        }

        // Are there enough remaining elements to be selected?
        // This test isn't required for the function to be correct, but
        // it can save a good amount of futile function calls.
        if ((elems.size() - margin) < (req_len - depth))
            return;

        // Try to select new elements to the right of the last selected one.
        for (unsigned long ii = margin; ii < elems.size(); ++ii) {
            pos[depth] = ii;
            combinations_recursive(elems, req_len, pos, depth + 1, ii + 1,ret);
        }
        return;
    }
};

MainWin::MainWin(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWin){
    ui->setupUi(this);


    auto t=new QTimer(this);
    t->setSingleShot(false);

    auto screenCapture=std::make_shared<CV_ScreenCapture>(0);
    CardImageAnalyzer c;
    auto mat=std::make_shared<cv::Mat>();

    std::vector<unsigned> deckCards;
    for (unsigned card = 0; card < 53; ++card) {
        deckCards.push_back(card);
    }

    connect(ui->pbMinus,&QPushButton::clicked,[this](){
        ui->pCount->setValue(ui->pCount->value()-1);
    });
    connect(ui->pbPlus,&QPushButton::clicked,[this](){
        ui->pCount->setValue(ui->pCount->value()+1);
    });



    QRect screenres = QApplication::screens().last()->geometry();
    auto l=new QLabel();
    l->move(QPoint(screenres.x(), screenres.y()));
    l->resize(screenres.width(), screenres.height());
    l->setScaledContents(true);
    l->showFullScreen();

    auto defaultFont=ui->percent->font();
    auto disabeledFont=defaultFont;
    disabeledFont.setPointSizeF(0.01);
    disabeledFont.setItalic(true);

    connect(t,&QTimer::timeout,[=](){
        ui->percent->setFont(disabeledFont);
        ui->l1->setVisible(false);
        ui->l2->setVisible(false);
        ui->l3->setVisible(false);

        auto setDisaplyed=[=](){
            ui->percent->setFont(defaultFont);
            for (auto& elem : {ui->l1,ui->l2,ui->l3}) {
                if(!elem->text().isEmpty()){
                    elem->setVisible(true);
                }
            }
        };
        auto setOdds=[=](const Odds& a){
            auto medianOdds=1.0/ui->pCount->value();
            ui->percent->setValue(50*a._WIN/medianOdds);
            ui->percent->setFont(defaultFont);
            auto values=a.values();
            auto valueIt=values.rbegin();
            for (auto& elem : {ui->l1,ui->l2,ui->l3}) {
                elem->setText("");
            }
            for (auto& elem : {ui->l1,ui->l2,ui->l3}) {
                if(valueIt==values.rend())break;
                elem->setVisible(true);
                elem->setText(QString::number(100*(*valueIt).first,'f',0)+" "+QString::fromStdString((*valueIt).second));
                valueIt++;
            }
        };


        screenCapture->readTo(*mat);
        auto captured=QtOpenCV::q_ref(*mat,QImage::Format::Format_ARGB32);
        cv::Mat capturedGray;
        cv::cvtColor(*mat,capturedGray, cv::COLOR_BGRA2GRAY);

        auto hand=c.hand(capturedGray,*mat);
        try {
            hand.waitForFinished();
        }  catch (...) {
            cv::imshow("hand bug",*mat);
            cv::waitKey(0);
        }
        auto handRes=hand.result();
        if(handRes.size()==2){
            auto table=c.table(capturedGray,*mat);
            try {
                table.waitForFinished();
            }  catch (...) {
                cv::imshow("table bug",*mat);
                cv::waitKey(0);
            }
            auto tableRes=table.result();

            /*proba*/{
                std::vector<PooID> playerHandWithTable;
                for (auto& card : handRes) {
                    playerHandWithTable.push_back(Cards::cardToPoodID(card));
                }
                for (auto& card : tableRes) {
                    playerHandWithTable.push_back(Cards::cardToPoodID(card));
                }

                if(playerHandWithTable.size()>=2){
                    if(cache&&(*cache.get())==playerHandWithTable){
                        setDisaplyed();
                    }else{
                        cache=std::make_shared<std::vector<PooID>>(playerHandWithTable);
                        auto oods=odds(playerHandWithTable,ui->pCount->value());
                        setOdds(oods);
                    }
                }
            }

        }
        l->setPixmap(QPixmap::fromImage(QtOpenCV::q_ref(*mat,QImage::Format::Format_ARGB32)));

    });
    t->start(500);

    setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    setAttribute(Qt::WA_TranslucentBackground);

    auto currentRect=geometry();
    currentRect.moveTo(1400,980);
    setGeometry(currentRect);
}


MainWin::~MainWin(){
    delete ui;
}

int main(int argc, char *argv[]){
    QApplication a(argc, argv);
    a.setOrganizationName("Nicolas SALMIERI");
    a.setOrganizationDomain("");
    a.setApplicationName("PockerPP");
    a.setWindowIcon(QIcon());

    MainWin w;
    w.show();
    return a.exec();
}
